PROFILES_DIRECTORY=/usr/share/postfix-profiles/

list_prepend_value() {
  list="$1"
  match_value="$2"
  prepend_value="$3"

  list=$(echo "$list" | sed "s/$match_value/$prepend_value $match_value/")

  echo "$list"
}

list_append_value() {
  list="$1"
  match_value="$2"
  append_value="$3"

  list=$(echo "$list" | sed "s/$match_value/$match_value $append_value/")

  echo "$list"
}

is_function() {
    command -v "$1" >/dev/null
}

run_optional_hook() {
    hook=$1

    if is_function "$hook"; then
        $hook
    fi
}

profiles() {
    for profile in "${PROFILES_DIRECTORY}"/*.sh; do
        profile_name=${profile%*.sh}
        profile_name=${profile_name##*/}
        echo "$profile_name"
    done
}

source_profiles() {
    for profile in $(profiles); do
        # shellcheck disable=1090
        . "$PROFILES_DIRECTORY/${profile}".sh
    done
}
